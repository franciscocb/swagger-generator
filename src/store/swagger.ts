import { GetterTree, ActionTree, MutationTree } from 'vuex'

export type RootState = ReturnType<typeof state>

export const state = () => ({
  swagger: null,
})

export const getters: GetterTree<RootState, RootState> = {
  swagger: (state) => state.swagger,

  tags: (state: RootState): string[] => {
    if (!state.swagger) {
      return []
    }

    const swg: any = state.swagger
    const outputTags: string[] = []
    outputTags.push(...swg.tags)

    console.log()

    for (let i in swg.paths) {
      const path = swg.paths[i]
      console.log(path)

      for (let method in path) {
        const pathMethod = path[method]

        if (pathMethod.tags) {
          for (let ii in pathMethod.tags) {
            if (!outputTags.includes(pathMethod.tags[ii])) {
              outputTags.push(pathMethod.tags[ii])
            }
          }
        }
      }
    }

    return outputTags.sort((a: string, b: string) => a.localeCompare(b))
  },

  controllerArray(state: RootState) {
    if (!state.swagger) {
      return []
    }

    const swg: any = state.swagger
    const outputArray: any[] = []
    for (let k in swg.paths) {
      outputArray.push({
        name: k,
        controllers: swg.paths[k],
      })
    }

    return outputArray
  },
}

export const mutations: MutationTree<RootState> = {
  swagger(state: any, payload) {
    state.swagger = payload
  },
}

export const actions: ActionTree<RootState, RootState> = {
  async load({ commit }) {
    console.log('loading swagger')

    const r = await fetch('/swagger.json')
    const swagger = await r.json()
    commit('swagger', swagger)
  },
}
